const expresiones = {
    codigo: /([0-9])/,
    titulo: /^[a-zA-Z0-9\_\-]{1,100}$/,
    autor: /^[a-zA-ZÀ-ÿ\s]{1,60}$/,
    editorial: /^[a-zA-Z0-9\_\-]{1,30}$/,
    fechaingreso: /^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/,
    fechapublicacion: /^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/,
}
const campos = {
    codigo: false,
    titulo: false,
    autor: false,
    editorial: false,
    fechaingreso:false,
    fechapublicacion:false
}

inputs.forEach(campo => {
    campo.addEventListener('keyup', (e) => {
        let valueInput = e.target.value;
        switch (e.target.id) {
            case 'codigo':
                campo.value = valueInput.replace(expresiones.codigo, '');
                alert("es valido el codigo");
                break;
            case 'titulo':
                campo.value = valueInput.replace(expresiones.titulo, '');
                alert("es valido el titulo");
                break;
            case 'autor':
                campo.value = valueInput.replace(expresiones.autor, '');
                alert("es valido el autor");
                break;
            case 'editorial':
                campo.value = valueInput.replace(expresiones.editorial, '');
                alert("es valido el editorial");

                break;
            case 'fechaingreso':
                if(fechaingreso>fechapublicacion){
                    alert("la fecha de ingreso es mayor asi que es valido");
                }
                break;
        }

    })
});
